package com.ctaiot;

import java.util.HashMap;

public class CloudPrintConfig extends HashMap<String, Object> {

    public static final String HOST_NAME = "host.name";
    public static final Object HOST_NAME_SCHEMA = String.class;

    public static final String HOST_NAME_PATH = "host.name.path";
    public static final Object HOST_NAME_PATH_SCHEMA = String.class;

    public static final String SDK_TOKEN = "sdk.token";
    public static final Object SDK_TOKEN_SCHEMA = String.class;

    public static final String BRAND_LIST = "brand.list";
    public static final Object BRAND_LIST_SCHEMA = String.class;

    public static final String BRAND_SAVE = "brand.save";
    public static final Object BRAND_SAVE_SCHEMA = String.class;

    public static final String FOLDER_LABEL_LIST = "folder.label.list";
    public static final Object FOLDER_LABEL_LIST_SCHEMA = String.class;

    public static final String CLIENT_ID_INFO = "client.id.info";
    public static final Object CLIENT_ID_INFO_SCHEMA = String.class;

    public static final String SEND_API_PRINT = "send.api.print";
    public static final Object SEND_API_PRINT_SCHEMA = String.class;

    public static final String SEND_PDF_PRINT = "send.pdf.print";
    public static final Object SEND_PDF_PRINT_SCHEMA = String.class;

    public static final String SEND_PIC_PRINT = "send.pic.print";
    public static final Object SEND_PIC_PRINT_SCHEMA = String.class;

    public static final String PRINT_URL = "print.url";
    public static final Object PRINT_URL_SCHEMA = String.class;

    public static final String LIST_PRINT_TASK = "list.print.task";
    public static final Object LIST_PRINT_TASK_SCHEMA = String.class;

    public static final String OPERATE_PRINT_TASK = "operate.print.task";
    public static final Object OPERATE_PRINT_TASK_SCHEMA = String.class;

    public static final String SEND_DATA_PRINT = "send.data.print";
    public static final Object SEND_DATA_PRINT_SCHEMA = String.class;


    public static final String MAX_RETRY_TIMES = "max.retry.times";
    public static final Object MAX_RETRY_TIMES_SCHEMA = Integer.class;
    public static final int DEFULT_MAX_RETRY_TIMES = 3;

    public static final String READ_TIMEOUT = "read.timeout";
    public static final Object READ_TIMEOUT_SCHEMA = Integer.class;
    public static final int DEFAULT_READ_TIMEOUT = 30 * 1000;

    public static final String CONNECTION_REQUEST_TIMEOUT = "connection.request.timeout";
    public static final Object CONNECTION_REQUEST_TIMEOUT_SCHEMA = Integer.class;
    public static final int DEFAULT_CONNECTION_REQUEST_TIMEOUT = 10 * 1000;

    public static final String CONNECTION_TIMEOUT = "connection.timeout";
    public static final Object CONNECTION_TIMEOUT_SCHEMA = Integer.class;
    public static final int DEFAULT_CONNECTION_TIMEOUT = 5 * 1000;

    public static final String SOCKET_TIMEOUT = "socket.timeout";
    public static final Object SOCKET_TIMEOUT_SCHEMA = Integer.class;
    public static final int DEFAULT_SOCKET_TIMEOUT = 10 * 1000;

    public static final String PRE_UPLOAD_RESOURCE = "pre.upload.resource";
    public static final Object PRE_UPLOAD_RESOURCE_SCHEMA = String.class;

    public static final String SEND_PIC_RESOURCE_PRINT = "send.pic.resource";
    public static final Object SEND_PIC_RESOURCE_PRINT_SCHEMA = String.class;

    private static CloudPrintConfig instance = new CloudPrintConfig();

    public CloudPrintConfig() {
        super(22);

        this.put(HOST_NAME, "https://dlabelplugga.ctaiot.com:9443");
        this.put(HOST_NAME_PATH, "/api");

        this.put(SDK_TOKEN, "/sdk/get/token.json");
        this.put(BRAND_LIST, "/brand/get_brand_list.json");
        this.put(BRAND_SAVE, "/brand/save_brand_setting.json");
        this.put(FOLDER_LABEL_LIST, "/folder/label/list.json");
        this.put(CLIENT_ID_INFO, "/sdk/get/client_id.json");
        this.put(SEND_API_PRINT, "/print/send_api_print.json");
        this.put(SEND_PDF_PRINT, "/print/send_pdf_url_print.json");
        this.put(SEND_PIC_PRINT, "/print/send_pic_url_print.json");
        this.put(PRINT_URL, "/sdk/get/print_url.json ");
        this.put(LIST_PRINT_TASK, "/print/list_print_task.json");
        this.put(OPERATE_PRINT_TASK, "/print/operate_print_task.json");
        this.put(PRE_UPLOAD_RESOURCE,"/sdk/pre_upload_resource.json");
        this.put(SEND_PIC_RESOURCE_PRINT,"/print/send_pic_rescource_print.json");
        this.put(SEND_DATA_PRINT,"/print/send_data_print.json");

        this.put(MAX_RETRY_TIMES, DEFULT_MAX_RETRY_TIMES);
        this.put(READ_TIMEOUT, DEFAULT_READ_TIMEOUT);
        this.put(CONNECTION_REQUEST_TIMEOUT, DEFAULT_CONNECTION_REQUEST_TIMEOUT);
        this.put(CONNECTION_TIMEOUT, DEFAULT_CONNECTION_TIMEOUT);
        this.put(SOCKET_TIMEOUT, DEFAULT_SOCKET_TIMEOUT);
    }

    public static CloudPrintConfig getInstance() {
        return instance;
    }

    public Integer getMaxRetryTimes() {
        return (Integer) this.get(MAX_RETRY_TIMES);
    }

    public Integer getReadTimeout() {
        return (Integer) this.get(READ_TIMEOUT);
    }

    public Integer getConnectionRequestTimeout() {
        return (Integer) this.get(CONNECTION_REQUEST_TIMEOUT);
    }

    public Integer getConnectionTimeout() {
        return (Integer) this.get(CONNECTION_TIMEOUT);
    }

    public Integer getSocketTimeout() {
        return (Integer) this.get(SOCKET_TIMEOUT);
    }


}
