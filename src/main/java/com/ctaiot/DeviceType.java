package com.ctaiot;

public enum DeviceType {

    PC(1, "PC"),
    AD(2, "AD"),
    IOS(3, "IOS"),
    MAC(4, "MAC"),
    WECHAT(5, "WECHAT"),
    ALIPAY(6, "ALIPAY");

    private Integer code;

    private String value;

    DeviceType(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
