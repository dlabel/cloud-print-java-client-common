package com.ctaiot.connection;

import com.ctaiot.resp.APIConnectionException;
import com.ctaiot.resp.APIRequestException;
import com.ctaiot.resp.ResponseWrapper;

import java.util.Map;

public interface IHttpClient {

    String CHARSET = "UTF-8";
    String CONTENT_TYPE_JSON = "application/json";
    String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";

    String RATE_LIMIT_QUOTA = "X-Rate-Limit-Limit";
    String RATE_LIMIT_Remaining = "X-Rate-Limit-Remaining";
    String RATE_LIMIT_Reset = "X-Rate-Limit-Reset";
    String CLOUD_PRINT_USER_AGENT = "Cloud-Print-API-Java-Client";

    /**
     * 成功标志
     */
    int RESPONSE_OK = 100;

    /**
     * 请求方式枚举类
     */
    enum RequestMethod {
        GET,
        POST,
        PUT,
        DELETE
    }

    /**
     * 连接io错误
     */
    String IO_ERROR_MESSAGE = "Connection IO error. \n"
            + "Can not connect to Cloud Print Server. "
            + "Please ensure your internet connection is ok. \n";
    /**
     * 连接超时
     */
    String CONNECT_TIMED_OUT_MESSAGE = "connect timed out. \n"
            + "Connect to Cloud Print Server timed out, and already retried some times. \n"
            + "Please ensure your internet connection is ok. \n";
    /**
     * 读取时间超时
     */
    String READ_TIMED_OUT_MESSAGE = "Read timed out. \n"
            + "Read response from Cloud Print Server timed out. \n"
            + "If this is a Push action, you may not want to retry. \n"
            + "It may be due to slowly response from Cloud Print server, or unstable connection. \n"
            ;


    /**
     * 设置连接超时时间,milliseconds
     */
    int DEFAULT_CONNECTION_TIMEOUT = (5 * 1000);

    /**
     * 设置读取超时时间,milliseconds
     */
    int DEFAULT_READ_TIMEOUT = (30 * 1000);
    /**
     * 设置默认最大重试次数
     */
    int DEFAULT_MAX_RETRY_TIMES = 3;



    ResponseWrapper sendGet(String url,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;

    ResponseWrapper sendGet(String url, String content,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;


    ResponseWrapper sendDelete(String url,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;

    ResponseWrapper sendDelete(String url, String content,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;

    ResponseWrapper sendPost(String url, String content,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;


    ResponseWrapper sendPut(String url, String content,Map<String, Object> header)
            throws APIConnectionException, APIRequestException;



}
