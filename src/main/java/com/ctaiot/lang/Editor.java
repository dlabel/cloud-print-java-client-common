package com.ctaiot.lang;

/**
 * 编辑器接口，常用于对于集合中的元素做统一编辑 此编辑器两个作用：
 *    1、如果返回值为null，表示此值被抛弃
 *    2、对对象做修改
 * @param <T>
 */
public interface Editor<T> {
    /**
     * 修改过滤后的结果
     *  形参:
     *  t – 被过滤的对象
     *  返回值:
     *  修改后的对象，如果被过滤返回null
     * @param t
     * @return t
     */
    T edit(T t);

}
