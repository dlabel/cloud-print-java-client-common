package com.ctaiot.resp;

 public class APIConnectionException extends Exception {
    private static final long serialVersionUID = -1964277709090141607L;

    private boolean readTimedOut = false;
    private int doneRetriedTimes = 0;

    public APIConnectionException(String message, Throwable e) {
        super(message, e);
    }

    public APIConnectionException(String message, Throwable e, int doneRetriedTimes) {
        super(message, e);
        this.doneRetriedTimes = doneRetriedTimes;
    }

    public APIConnectionException(String message, Throwable e, boolean readTimedout) {
        super(message, e);
        this.readTimedOut = readTimedout;
    }

    public boolean isReadTimedOut() {
        return readTimedOut;
    }
    
    public int getDoneRetriedTimes() {
        return this.doneRetriedTimes;
    }
}


