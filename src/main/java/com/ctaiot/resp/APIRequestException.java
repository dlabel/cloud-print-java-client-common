package com.ctaiot.resp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class APIRequestException extends Exception implements IRateLimiting {

    private static final long serialVersionUID = 2432505292560619144L;
    protected static Gson _gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    private final ResponseWrapper responseWrapper;

    public APIRequestException(ResponseWrapper responseWrapper) {
        super(responseWrapper.responseContent);
        this.responseWrapper = responseWrapper;
    }

    public int getStatus() {
        return this.responseWrapper.responseCode;
    }


    public int getErrorCode() {
        ResponseWrapper.ErrorEntity eo = getErrorObject();
        if (null != eo) {
            return eo.code;
        }
        return -1;
    }

    public String getErrorMessage() {
        ResponseWrapper.ErrorEntity eo = getErrorObject();
        if (null != eo) {
            return eo.message;
        }
        return null;
    }

    @Override
    public String toString() {
        return _gson.toJson(this);
    }

    private ResponseWrapper.ErrorEntity getErrorObject() {
        return this.responseWrapper.error;
    }


    @Override
    public int getRateLimitQuota() {
        return this.responseWrapper.rateLimitQuota;
    }

    @Override
    public int getRateLimitRemaining() {
        return this.responseWrapper.rateLimitRemaining;
    }

    @Override
    public int getRateLimitReset() {
        return this.responseWrapper.rateLimitReset;
    }
}

