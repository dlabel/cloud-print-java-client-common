package com.ctaiot.resp;

public interface IRateLimiting {

    int getRateLimitQuota();
    
    int getRateLimitRemaining();
    
    int getRateLimitReset();
    
}

