package com.ctaiot.utils;


import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * bean转换Map操作类
 */
public class BeanUtil {

    /**
     * 对象转Map
     *
     * @param bean bean对象
     * @return Map<String, Object>
     * @throws IntrospectionException 转化异常
     * @throws InvocationTargetException 反射强制类型异常
     * @throws IllegalAccessException 类型转换异常
     */
    public static Map<String, Object> beanToMap(Object bean) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        return beanToMap(bean, false, false);
    }

    /**
     * 对象转Map
     *
     * @param bean         bean对象
     * @param isIgnoreNull 是否忽略值为空的字段
     * @return Map<String, Object>
     * @throws IntrospectionException 转化异常
     * @throws InvocationTargetException 反射强制类型异常
     * @throws IllegalAccessException 类型转换异常
     */
    public static Map<String, Object> beanToMap(Object bean, boolean isIgnoreNull, boolean isIgnoreUnderLine) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        return beanToMap(bean, new HashMap<>(), isIgnoreNull, isIgnoreUnderLine);
    }

    /**
     * 对象转Map
     *
     * @param bean         bean对象
     * @param target       目标的Map
     * @param isIgnoreNull 是否忽略值为空的字段
     * @return Map<String, Object>
     * @throws IntrospectionException 转化异常
     * @throws InvocationTargetException 反射强制类型异常
     * @throws IllegalAccessException 类型转换异常
     */
    public static Map<String, Object> beanToMap(Object bean, Map<String, Object> target,
                                                boolean isIgnoreNull, boolean isIgnoreUnderLine) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        if (bean == null) {
            return null;
        }

        BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
        PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor descriptor : descriptors) {
            String key = descriptor.getName();
            if (key.compareToIgnoreCase("class") == 0) {
                continue;
            }
            Method getter = descriptor.getReadMethod();
            Object value = getter != null ? getter.invoke(bean) : null;

            if (isIgnoreNull == false || (null != value && false == value.equals(bean))) {
                if (key != null && !key.equals("token")) {
                    if (isIgnoreUnderLine) {
                        key = convert(key);
                    }
                    target.put(key, value);
                }
            }
        }
        return target;
    }


    public static String convert(String str) {
        Pattern compile = Pattern.compile("[A-Z]");
        Matcher matcher = compile.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

}
