package com.ctaiot.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {
    /**
     * 存储用户编号信息
     *
     * @param userId
     * @throws IOException
     */
    public void saveUserId(File file,String userId) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter writer = new FileWriter(file);
        BufferedWriter out = new BufferedWriter(writer);
        out.write(userId);
        out.flush();
        out.close();
        writer.close();

    }

    /**
     * 获取用户编号信息
     *
     * @return 用户编号
     * @throws IOException
     */
    public String getUserId(String userId) throws IOException {
        File directory = new File("");

        File file = new File(directory.getAbsolutePath() + "/demo.txt");

        if (!file.exists()){
            this.saveUserId(file,userId);
            return userId;
        }

        StringBuilder userIdStr = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(directory.getAbsolutePath() + "/demo.txt"))) {
            String text;
            while ((text = br.readLine()) != null){
                userIdStr.append(text);
            }
        }

        return userIdStr.toString();
    }
}
