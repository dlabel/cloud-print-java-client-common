package com.ctaiot.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.TreeMap;

public class Md5Util {

    public static String formatSignContent(TreeMap<String, Object> treeMap) {
        StringBuffer sb = new StringBuffer();
        for (String key : treeMap.keySet()) {
            Object value = treeMap.get(key);
            //剔除value为空的参数
            if (value == null || value.equals("")) {
                continue;
            }
            sb.append("{").append(key).append("}").append("{").append(value.toString()).append("}");
        }
        return sb.toString();
    }

    public static String sign(String signContext, String appSecret) {
        String signStr = signContext.concat("{").concat(appSecret).concat("}");
        return DigestUtils.md5Hex(signStr);
    }

}
